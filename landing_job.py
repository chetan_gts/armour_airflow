from airflow.hooks.postgres_hook import PostgresHook
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import os
import shutil


pg_hook = PostgresHook(postgres_conn_id='postgres_local')
conn = pg_hook.get_conn()
cursor = conn.cursor()

'''
def update_etl_control_table(**kwargs):
    conn = kwargs['conn']
    cursor = kwargs['cursor']

    cursor.execute("insert into public.airflow_etl_control_table (landing_updated_time) values (current_timestamp)")
    #conn.commit()
'''
    
def load_csv_to_postgres(file_path, filename, table_name, cursor, start_time, error_path):
    # Use a staging area within your transaction
    sql_copy = f"""
    COPY {table_name} ("A_Number","B_Number","C_Number","Originating_MDL_Location","Originating_MDL_Dial_Code","MDL_Destination","MDL_Destination_Dial_Code","Date","Time","Answered_Time","Unanswered_Time","Call_Status","Release_Cause","PDD","OutpulseNumber") FROM STDIN WITH (FORMAT csv, HEADER true, DELIMITER ',');
    """
    
    try:
        with open(file_path, 'r') as file:
            cursor.copy_expert(sql=sql_copy, file=file) 
            inserted_row_count = cursor.rowcount

        with open(file_path, 'r') as file:
            next(file)  # Skip the header row if your CSV has a header
            row_count = sum(1 for row in file)  # Efficiently count the number of rows

    # Now, perform the COPY operation
        

        file_processed_logs_query = f"""insert into public.fileprocessed_logs(processed_date,file_name,filecount,tablecount) values (current_timestamp, '{filename}', {row_count}, {inserted_row_count})"""
        
        # Update the filename column for the newly inserted rows

        sql_update = f"""
        UPDATE {table_name}
        SET "file_name" = '{filename}',
            "create_date" = '{start_time}' ,
            "update_date" = '{start_time}'
        WHERE "file_name" IS NULL;
        """
        cursor.execute(sql_update)
        #conn.commit()

        cursor.execute(file_processed_logs_query)
        #conn.commit()
    
    except Exception as e:
        # Log error and move the file to the error folder
        shutil.move(file_path, error_path)

def process_files(**kwargs):
    CSV_FOLDER = '/opt/airflow/source'
    DST_FOLDER = '/opt/airflow/destination'
    ERROR_FOLDER = '/opt/airflow/error'
    csv_files = os.listdir(CSV_FOLDER)

    conn = kwargs['conn']
    cursor = kwargs['cursor']

    cursor.execute("insert into public.airflow_etl_control_table (landing_updated_time) values (current_timestamp)")
    conn.commit()

    time_query = """select current_timestamp"""
    cursor.execute(time_query)
    start_time = cursor.fetchall()
    start_time = start_time[0][0]

    cdr_og_count_before = """select count(*) from public.cdr_og"""
    cdr_ic_count_before = """select count(*) from public.cdr_ic"""

    cursor.execute(cdr_og_count_before)
    cdr_og_count_before = cursor.fetchall()
    cdr_og_count_before = int(cdr_og_count_before[0][0])

    cursor.execute(cdr_ic_count_before)
    cdr_ic_count_before = cursor.fetchall()
    cdr_ic_count_before = cdr_ic_count_before[0][0]

    files_list = {}

    for file_name in csv_files:
        file_path = os.path.join(CSV_FOLDER, file_name)
        dest_path = os.path.join(DST_FOLDER, file_name)
        error_path = os.path.join(ERROR_FOLDER, file_name)
        
        # Determine the table based on the filename
        if 'SBC_CDRs_OG_TEX1B' in file_name:
            table_name = 'cdr_og'
        
        elif 'SBC_CDRs_Trunk-1_IC_TEX1B' in file_name:
            table_name = 'cdr_ic'
        
        else:
            continue  # Skip files that do not match any pattern
        
        load_csv_to_postgres(file_path, file_name, table_name, cursor,start_time,error_path )
        #file_path, filename, table_name, cursor, start_time, error_path
        files_list[file_path] = dest_path

    cdr_og_count_after = """select count(*) from public.cdr_og"""
    cdr_ic_count_after = """select count(*) from public.cdr_ic"""

    cursor.execute(cdr_og_count_after)
    cdr_og_count_after = cursor.fetchall()
    cdr_og_count_after = int(cdr_og_count_after[0][0])

    cursor.execute(cdr_ic_count_after)
    cdr_ic_count_after = cursor.fetchall()
    cdr_ic_count_after = int(cdr_ic_count_after[0][0])
    
    og_count = cdr_og_count_after - cdr_og_count_before
    ic_count = cdr_ic_count_after - cdr_ic_count_before

    query_batch_logs = f"""INSERT INTO public.batch_logs (start_time, end_time, count_of_records_og, count_of_records_ic) VALUES ('{start_time}', CURRENT_TIMESTAMP, {og_count}, {ic_count});"""

    cursor.execute(query_batch_logs)
    print(files_list)
    

    for key in  files_list:   
        shutil.move(key, files_list[key])
    
    conn.commit()

# Integrate this with your Airflow DAG as before

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2024, 4, 28),
    'email': ['your_email@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'csv_to_postgres_with_filename',
    default_args=default_args,
    description='Load CSV files into PostgreSQL database with source filename',
    schedule_interval=None,
    catchup=False,
)

process_og_files_task = PythonOperator(
    task_id='process_og_csv_files_with_filename',
    python_callable=process_files,
    op_kwargs = {'conn':conn, 'cursor':cursor},
    dag=dag,
)


process_og_files_task

# The rest of your Airflow DAG to call this function would be similar to previous examples
