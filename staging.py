from airflow.hooks.postgres_hook import PostgresHook
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta

pg_hook = PostgresHook(postgres_conn_id='postgres_local')
conn = pg_hook.get_conn()
cursor = conn.cursor()


def insert_data_into_staging(**kwargs):
    conn = kwargs['conn']
    cursor = kwargs['cursor']

    sql_og_staging_query = """
    INSERT INTO staging.stg_cdr_og (
    "A_Number", "B_Number", "Originating_MDL_Location", "Originating_MDL_Dial_Code", 
    "MDL_Destination", "MDL_Destination_Dial_Code", "Date", "Time", "Answered_Time", 
    "Unanswered_Time", "Call_Status", "Release_Cause", create_date, update_date, file_name
    )
    WITH landing_hist_date AS (
        SELECT MIN(landing_updated_time) AS min_start_time
        FROM public.airflow_etl_control_table
        WHERE landing_updated_time > (
            SELECT MAX(update_timestamp)
            FROM public.staging_control_table
        )
    )
    SELECT 
        "A_Number", "B_Number", "Originating_MDL_Location", "Originating_MDL_Dial_Code", 
        "MDL_Destination", "MDL_Destination_Dial_Code", "Date"::date, "Time"::time, "Answered_Time"::float, 
        "Unanswered_Time"::float, "Call_Status", "Release_Cause", create_date::timestamp, update_date, file_name
    FROM 
        public.cdr_og, landing_hist_date
    WHERE 
        create_date::timestamp >= landing_hist_date.min_start_time

    """

    sql_ic_staging_query = """
    INSERT INTO staging.stg_cdr_ic (
    "A_Number", "B_Number", "Originating_MDL_Location", "Originating_MDL_Dial_Code", 
    "MDL_Destination", "MDL_Destination_Dial_Code", "Date", "Time", "Answered_Time", 
    "Unanswered_Time", "Call_Status", "Release_Cause", create_date, update_date, file_name
    )
    WITH landing_hist_date AS (
        SELECT MIN(landing_updated_time) AS min_start_time
        FROM public.airflow_etl_control_table
        WHERE landing_updated_time > (
            SELECT MAX(update_timestamp)
            FROM public.staging_control_table
        )
    )
    SELECT 
        "A_Number", "B_Number", "Originating_MDL_Location", "Originating_MDL_Dial_Code", 
        "MDL_Destination", "MDL_Destination_Dial_Code", "Date"::date, "Time"::time, "Answered_Time"::float, 
        "Unanswered_Time"::float, "Call_Status", "Release_Cause", create_date::timestamp, update_date, file_name
    FROM 
        public.cdr_ic, landing_hist_date
    WHERE 
        create_date::timestamp >= landing_hist_date.min_start_time

    """

    cursor.execute(sql_og_staging_query)
    cursor.execute(sql_ic_staging_query)


    query_insert_stg_control_table = "INSERT INTO public.staging_control_table (update_timestamp) SELECT max(create_date)::timestamp FROM staging.stg_cdr_og;"
    cursor.execute(query_insert_stg_control_table)
    conn.commit()
    

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2024, 4, 28),
    'email': ['your_email@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'insert_into_staging_table',
    default_args=default_args,
    description='Load CSV files into landing table with source filename',
    schedule_interval=None,
    catchup=False,
)

insert_into_staging_table = PythonOperator(
    task_id='insert_into_stg_table_and_control_table',
    python_callable=insert_data_into_staging,
    op_kwargs = {'conn':conn, 'cursor':cursor},
    dag=dag,
)








#select id from public.airflow_etl_control_table where landing_updated_time = (select max(update_date) from public.staging_control_table)