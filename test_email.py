from airflow.operators.email_operator import EmailOperator
from datetime import datetime, timedelta
from airflow import DAG




default_args = {
    "owner": "chetan",
    "start_date": datetime(2024, 4, 28),
    'email': ['chetanmsvec@gmail.com'],
    'email_on_failure': True,
}

with DAG(dag_id="test_mail",
        schedule_interval=None,
        default_args=default_args,
    ) as dag:

    test_email = EmailOperator(
       task_id='email_test',
       to='chetanmsvec@gmail.com',
       subject='Airflow Alert !!!',
       html_content="""<h1>Testing Email using Airflow</h1>""",
    )
    
test_email